<?php
/**
 * Database Configuration
 *
 * All of your system's database connection settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/DbConfig.php.
 *
 * @see craft\config\DbConfig
 */


 function checksub($domain) {
   $exp = explode('.', $domain);
   if(count($exp) > 2) {
     return true;
   } else{
     return false;
   }
 }
// echo "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
 if(checksub("http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}") ) {
   $sub = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
 } else {
   $sub = '';
 }

 // echo $sub;

  if($sub == 'www' or $sub == '' or $sub == 'uk' or $sub == 'gb'){
    $db = getenv('DB_DATABASE');
  }elseif($sub == ''){
    $db = getenv('DB_DATABASE');
  }else{
    $db = $sub . '_declare';
  }


return [
    'driver' => getenv('DB_DRIVER'),
    'server' => getenv('DB_SERVER'),
    'user' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'database' => $db,
    'schema' => getenv('DB_SCHEMA'),
    'tablePrefix' => getenv('DB_TABLE_PREFIX'),
    'port' => getenv('DB_PORT')
];
