<?php

  /*
    DECLARES v2 (09/2020)
    -------------------
    This script is used to add the following new sections
    to a country declaration:
      - About page
      - Resources page
      - Events page
      - News page
      - Navigation/Menu
  */

  // Load Craft, taken from index.php
  define('CRAFT_BASE_PATH', dirname(__DIR__));
  define('CRAFT_VENDOR_PATH', CRAFT_BASE_PATH.'/vendor');
  require_once CRAFT_VENDOR_PATH.'/autoload.php';
  if (class_exists('Dotenv\Dotenv') && file_exists(CRAFT_BASE_PATH.'/.env')) {
      (new Dotenv\Dotenv(CRAFT_BASE_PATH))->load();
  }
  define('CRAFT_ENVIRONMENT', getenv('ENVIRONMENT') ?: 'production');
  $app = require CRAFT_VENDOR_PATH.'/craftcms/cms/bootstrap/web.php';
  echo " Using database: {$app->config->getDb()->database}";

  use craft\elements\GlobalSet;
  use craft\db\Query;
  use craft\models\FieldGroup;
  use craft\models\CategoryGroup;
  use craft\models\CategoryGroup_SiteSettings;
  use craft\models\FieldLayoutTab;
  use craft\fields\PlainText;
  use craft\fields\Assets;
  use craft\fields\Entries;
  use craft\fields\Date;
  use craft\fields\Url;
  use craft\fields\Categories;
  use craft\fields\Lightswitch;

  $userSession = Craft::$app->getUser();
  $isAdmin = $userSession->getIsAdmin();
  if ($isAdmin) {
    echo '<br /><br /> Creating categories...';
    createNewsCategory();
    echo '<br /> Creating sections...';
    createChannelSection('Events', 'events');
    createChannelSection('News', 'news');
    createChannelSection('Speakers', 'speakers');
    echo '<br /> Creating pages...';
    createGlobalPage('About Page', 'aboutPage');
    createGlobalPage('Events Page', 'eventsPage');
    createGlobalPage('News Page', 'newsPage');
    createGlobalPage('Resources Page', 'resourcesPage');
    createGlobalPage('Navigation', 'navigation');
    createGlobalPage('Settings', 'settings');
    echo '<br /> Creating fields...';
    createFields();
    echo '<br /> Assigning fields...';
    addFieldsToAboutPage();
    addFieldsToEventsPage();
    addFieldsToNewsPage();
    addFieldsToResourcesPage();
    addFieldsToNavigation();
    addFieldsToSettings();
    addFieldsToEventsSection();
    addFieldsToNewsSection();
    addFieldsToSpeakersSection();

  } else {
    echo '<br /><br />Unauthorized. Make sure you\'re logged in.';
    return false;
  }

  /* Create category sections
    - News category
  */

  /* Create fields
    - Page Title - Plain Text (common)
    - Body Text - Redactor (common)
    - Image - Assets (common)
    - Steering group - Redactor (about)
    - Why Declare - Redactor (about)
    - Support Us - Redactor (about)
    - Contact - Redactor (about)
    - Resources - Redactor (resources)
    - Useful links - Redactor (resources)
    - Events - Entries (events)
    - Description - Redactor (event)
    - Date - Date/Time (event)
    - Location - Plain text (event)
    - Speakers - Entries (event)
    - Vimeo URL - URL (speaker)
    - Presentation slides - Assets (speaker)
    - Category - Categories (news)
  */

  /* Create sections
    - Event - Channel
    - News - Channel
    - Speakers - Channel
  */

  /* Create global sections
    - About Page
    - Events Page
    - Resources Page
    - News Page
  */

  function errorCheck($errors) {
    foreach ($errors as $error) {
      echo "<br /> {$error[0]}";
    }
  }

  function createNewsCategory() {
    $group = new CategoryGroup();
    $group->name = 'News';
    $group->handle = 'news';

    $allSiteSettings = [];
    foreach (Craft::$app->getSites()->getAllSites() as $site) {
      $siteSettings = new CategoryGroup_SiteSettings();
      $siteSettings->siteId = $site->id;
      $siteSettings->hasUrls = 1;
      $siteSettings->uriFormat = '/news/category/{slug}';
      $siteSettings->template = 'news';
      $allSiteSettings[$site->id] = $siteSettings;
    }

    $group->setSiteSettings($allSiteSettings);
    $isSaved = Craft::$app->categories->saveGroup($group);

    $errors = $group->getErrors();
    errorCheck($errors);
  }

  function createFields() {
    $getFields = Craft::$app->getFields();
    $getSections = Craft::$app->getSections();
    $fields = [];

    function createPlainTextField($name, $handle, $groupId, $instructions = ''){
      $field = new PlainText([
        'groupId' => $groupId,
        'name' => $name,
        'handle' => $handle,
        'instructions' => $instructions
      ]);
      return $field;
    }

    function createUrlField($name, $handle, $groupId, $instructions = ''){
      $field = new Url([
        'groupId' => $groupId,
        'name' => $name,
        'handle' => $handle,
        'instructions' => $instructions
      ]);
      return $field;
    }

    function createSwitchField($name, $handle, $groupId, $instructions = '', $default = '0'){
      $field = new Lightswitch([
        'groupId' => $groupId,
        'name' => $name,
        'handle' => $handle,
        'instructions' => $instructions,
        'default' => $default
      ]);
      return $field;
    }

    function createRedactorField($name, $handle, $groupId){
      $field = Craft::$app->getFields()->createField([
        'type' => 'craft\\redactor\\Field',
        'groupId' => $groupId,
        'name' => $name,
        'handle' => $handle
      ]);
      return $field;
    }

    function createEntriesField($name, $handle, $groupId, $sourceId){
      $field = new Entries([
        'groupId' => $groupId,
        'name' => $name,
        'handle' => $handle,
        'sources' => ["section:{$sourceId}"]
      ]);
      return $field;
    }

    $commonGroup = (new Query())
        ->select('id')
        ->from('fieldgroups')
        ->where(['name' => 'Common'])
        ->one();

    $aboutGroup = new FieldGroup();
    $aboutGroup->name = 'About';
    $aboutGroupSaved = $getFields->saveGroup($aboutGroup);

    $resourcesGroup = new FieldGroup();
    $resourcesGroup->name = 'Resources';
    $resourcesGroupSaved = $getFields->saveGroup($resourcesGroup);

    $eventsGroup = new FieldGroup();
    $eventsGroup->name = 'Events';
    $eventsGroupSaved = $getFields->saveGroup($eventsGroup);

    $newsGroup = new FieldGroup();
    $newsGroup->name = 'News';
    $newsGroupSaved = $getFields->saveGroup($newsGroup);

    $navGroup = new FieldGroup();
    $navGroup->name = 'Navigation';
    $navGroupSaved = $getFields->saveGroup($navGroup);

    $settingsGroup = new FieldGroup();
    $settingsGroup->name = 'Settings';
    $settingsGroupSaved = $getFields->saveGroup($settingsGroup);

    $fields[] = createPlainTextField('Page Title', 'pageTitle', $commonGroup['id']);
    $fields[] = createRedactorField('Body Text', 'bodyText', $commonGroup['id']);
    $fields[] = createRedactorField('Steering Group', 'steeringGroup', $aboutGroup['id']);
    $fields[] = createRedactorField('Why Declare', 'whyDeclare', $aboutGroup['id']);
    $fields[] = createRedactorField('Support Us', 'supportUs', $aboutGroup['id']);
    $fields[] = createRedactorField('Contact', 'contact', $aboutGroup['id']);
    $fields[] = createRedactorField('Resources', 'resources', $resourcesGroup['id']);
    $fields[] = createRedactorField('Useful Links', 'usefulLinks', $resourcesGroup['id']);

    $eventsSectionUid = $getSections->getSectionByHandle('events')['uid'];
    $speakersSectionUid = $getSections->getSectionByHandle('speakers')['uid'];
    $fields[] = createEntriesField('Events', 'events', $eventsGroup['id'], $eventsSectionUid);
    $fields[] = createEntriesField('Speakers', 'speakers', $eventsGroup['id'], $speakersSectionUid);
    $fields[] = createRedactorField('Description', 'description', $eventsGroup['id']);
    $fields[] = createPlainTextField('Location', 'location', $eventsGroup['id']);
    $fields[] = createUrlField('Vimeo URL', 'vimeoUrl', $eventsGroup['id']);

    $v = Craft::$app->volumes->getVolumeByHandle('uploads');
    $folders = Craft::$app->assets->getFolderTreeByVolumeIds([$v->id]);
    $uploadsFolder = "folder:{$folders[0]->uid}";

    $fields[] = new Assets([
      'groupId' => $commonGroup['id'],
      'name' => 'Image',
      'handle' => 'image',
      'restrictFiles' => 1,
      'allowedKinds' => ['image'],
      'limit' => 1,
      'defaultUploadLocationSource' => $uploadsFolder,
      'defaultUploadLocationSubpath' => '',
      'sources' => [$uploadsFolder],
      'source' => null
    ]);

    $fields[] = new Date([
      'groupId' => $eventsGroup['id'],
      'name' => 'Date',
      'handle' => 'date',
      'showDate' => 1,
      'showTime' => 1,
      'minuteIncrement' => 15
    ]);

    $fields[] = new Assets([
      'groupId' => $eventsGroup['id'],
      'name' => 'Presentation slides',
      'handle' => 'presentationSlides',
      'restrictFiles' => 1,
      'allowedKinds' => ['pdf', 'powerpoint', 'text', 'word'],
      'limit' => 1,
      'defaultUploadLocationSource' => $uploadsFolder,
      'defaultUploadLocationSubpath' => '',
      'sources' => [$uploadsFolder],
      'source' => null
    ]);

    $fields[] = createPlainTextField(
      'Filter Label',
      'filterLabel',
      $newsGroup['id'],
      'Label to filter news by e.g. region'
    );

    $fields[] = createRedactorField('Newsletter Signup Text', 'newsletterSignupText', $settingsGroup['id']);
    $fields[] = createPlainTextField(
      'Newsletter Link Text',
      'newsletterLinkText',
      $settingsGroup['id'],
      'e.g. Join the newsletter'
    );
    $fields[] = createUrlField(
      'Newsletter Link URL',
      'newsletterLinkUrl',
      $settingsGroup['id'],
      'Link to an external Mailchimp (or other) signup form'
    );
    $newsCategoryUid = Craft::$app->getCategories()->getGroupByHandle('news')['uid'];
    $fields[] = new Categories([
      'groupId' => $newsGroup['id'],
      'name' => 'Categories',
      'handle' => 'categories',
      'source' => "group:{$newsCategoryUid}"
    ]);

    $fields[] = createSwitchField('Visible', 'visible', $navGroup['id']);
    $fields[] = createSwitchField('About Visible', 'aboutVisible', $navGroup['id']);
    $fields[] = createSwitchField('News Visible', 'newsVisible', $navGroup['id']);
    $fields[] = createSwitchField('Events Visible', 'eventsVisible', $navGroup['id']);
    $fields[] = createSwitchField('Resources Visible', 'resourcesVisible', $navGroup['id']);
    $fields[] = createPlainTextField('Parent Site Title', 'parentTitle', $navGroup['id']);
    $fields[] = createUrlField('Parent Site URL', 'parentUrl', $navGroup['id']);
    $fields[] = createSwitchField('Site status', 'siteStatus', $settingsGroup['id'], 'Turn site on/off', '1');

    foreach ($fields as $field) {
      $isSaved = $getFields->saveField($field);
      $errors = $field->getErrors();
      foreach ($errors as $error) {
        echo $error[0];
      }
    }
  }

  function createChannelSection($name, $handle) {
    $section = new \craft\models\Section([
      'name' => $name,
      'handle' => $handle,
      'type' => \craft\models\Section::TYPE_CHANNEL,
      'siteSettings' => [
        new \craft\models\Section_SiteSettings([
          'siteId' => Craft::$app->sites->getPrimarySite()->id,
          'enabledByDefault' => true,
          'hasUrls' => true,
          'uriFormat' => "{$handle}/{slug}",
          'template' => "{$handle}/_entry",
        ]),
      ]
    ]);

    $isSaved = Craft::$app->sections->saveSection($section);

    $errors = $section->getErrors();
    foreach ($errors as $error) {
      echo "<br /> {$error[0]}";
    }
  }

  function createGlobalPage($name, $handle) {
    $globalSet = new GlobalSet();
    $globalSet->name = $name;
    $globalSet->handle = $handle;
    $globalSet = Craft::$app->globals->saveSet($globalSet);
  }

  function addFieldsToAboutPage() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('pageTitle')["id"];
    $newFields[] = $getFields->getFieldByHandle('bodyText')["id"];
    $newFields[] = $getFields->getFieldByHandle('steeringGroup')["id"];
    $newFields[] = $getFields->getFieldByHandle('whyDeclare')["id"];
    $newFields[] = $getFields->getFieldByHandle('supportUs')["id"];
    $newFields[] = $getFields->getFieldByHandle('contact')["id"];
    addGlobalSetToPage($newFields, 'aboutPage');
  }

  function addFieldsToEventsPage() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('pageTitle')["id"];
    $newFields[] = $getFields->getFieldByHandle('bodyText')["id"];
    addGlobalSetToPage($newFields, 'eventsPage');
  }

  function addFieldsToNewsPage() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('pageTitle')["id"];
    $newFields[] = $getFields->getFieldByHandle('bodyText')["id"];
    $newFields[] = $getFields->getFieldByHandle('filterLabel')["id"];
    addGlobalSetToPage($newFields, 'newsPage');
  }

  function addFieldsToResourcesPage() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('pageTitle')["id"];
    $newFields[] = $getFields->getFieldByHandle('bodyText')["id"];
    $newFields[] = $getFields->getFieldByHandle('resources')["id"];
    $newFields[] = $getFields->getFieldByHandle('usefulLinks')["id"];
    addGlobalSetToPage($newFields, 'resourcesPage');
  }

  function addFieldsToNavigation() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('visible')["id"];
    $newFields[] = $getFields->getFieldByHandle('aboutVisible')["id"];
    $newFields[] = $getFields->getFieldByHandle('eventsVisible')["id"];
    $newFields[] = $getFields->getFieldByHandle('newsVisible')["id"];
    $newFields[] = $getFields->getFieldByHandle('resourcesVisible')["id"];
    $newFields[] = $getFields->getFieldByHandle('parentTitle')["id"];
    $newFields[] = $getFields->getFieldByHandle('parentUrl')["id"];
    addGlobalSetToPage($newFields, 'navigation');
  }

  function addFieldsToSettings() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('siteStatus')["id"];
    $newFields[] = $getFields->getFieldByHandle('newsletterSignupText')["id"];
    $newFields[] = $getFields->getFieldByHandle('newsletterLinkText')["id"];
    $newFields[] = $getFields->getFieldByHandle('newsletterLinkUrl')["id"];
    addGlobalSetToPage($newFields, 'settings');
  }

  function addGlobalSetToPage($fieldIds, $handle) {
    // Create new global set with new fields
    $globalSetLayout = Craft::$app->fields->assembleLayout(
      array('Content' => $fieldIds)
    );

    // Add the new global set to page
    $globalSet = Craft::$app->globals->getSetByHandle($handle);
    $globalSet->setFieldLayout($globalSetLayout);
    return Craft::$app->globals->saveSet($globalSet);
  }

  function addFieldsToEventsSection() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('description');
    $newFields[0]->sortOrder = 0;
    $newFields[] = $getFields->getFieldByHandle('image');
    $newFields[1]->sortOrder = 1;
    $newFields[] = $getFields->getFieldByHandle('date');
    $newFields[2]->sortOrder = 2;
    $newFields[] = $getFields->getFieldByHandle('location');
    $newFields[3]->sortOrder = 3;
    $newFields[] = $getFields->getFieldByHandle('speakers');
    $newFields[4]->sortOrder = 4;
    addFieldSetToSection($newFields, 'Events', 'events');
  }

  function addFieldsToNewsSection() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('bodyText');
    $newFields[0]->sortOrder = 0;
    $newFields[] = $getFields->getFieldByHandle('image');
    $newFields[1]->sortOrder = 1;
    $newFields[] = $getFields->getFieldByHandle('categories');
    $newFields[2]->sortOrder = 2;
    addFieldSetToSection($newFields, 'News', 'news');
  }

  function addFieldsToSpeakersSection() {
    $getFields = Craft::$app->getFields();
    $newFields = [];
    $newFields[] = $getFields->getFieldByHandle('vimeoUrl');
    $newFields[0]->sortOrder = 0;
    $newFields[] = $getFields->getFieldByHandle('presentationSlides');
    $newFields[1]->sortOrder = 1;
    addFieldSetToSection($newFields, 'Speakers', 'speakers');
  }

  function addFieldSetToSection($fields, $name, $handle) {
    $entries = Craft::$app->sections->getEntryTypesByHandle($handle);
    $sectionEntryType = $entries[0];
    $sectionFieldLayout = $sectionEntryType->getFieldLayout();
    $sectionTab = new FieldLayoutTab(["name" => "{$name} channel"]);
    $sectionTab->setFields($fields);
    $sectionFieldLayout->setTabs([$sectionTab]);
    return (Craft::$app->fields->saveLayout($sectionFieldLayout) && Craft::$app->sections->saveEntryType($sectionEntryType));
  }

  echo "<br /> Updating project config...";
  // echo shell_exec('cd ../; ./craft project-config/rebuild');

  use Composer\Console\Application;
  use Symfony\Component\Console\Input\ArrayInput;

  // Composer\Factory::getHomeDir() method 
  // needs COMPOSER_HOME environment variable set
  putenv('COMPOSER_HOME=' . __DIR__ . '/vendor/bin/composer');

  // call `composer install` command programmatically
  $input = new ArrayInput(array('command' => 'craft-rebuild-project-config'));
  $application = new Application();
  $application->setAutoExit(false); // prevent `$application->run` method from exitting the script
  $application->run($input);

  echo "<br /><br /><strong>Update complete!</strong>";
?>
