<?php
$servername = "localhost";
$username = "root";
$password = "root";
$file_location = getcwd() . "/" . "db_clone.sql";
$dblast = "_declare";

$conn = new mysqli($servername, $username, $password);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$strJsonFileContents = file_get_contents(getcwd()."/"."ico.json");
$array = json_decode($strJsonFileContents, true);

foreach ($array as $key => $value) {

  $code = strtolower($value['Code']);
  // $dbName = $code . $dblast;
  // $sql = "CREATE DATABASE " . $dbName;
  //
  // if ($conn->query($sql) === TRUE) {
  //     echo $dbName." created successfully";
  // } else {
  //     echo "Error creating database: " . $conn->error;
  // }

  echo $code.'<br>';

  // restoreDatabaseTables($servername, $username, $password, $dbName, $file_location);

}

$conn->close();


function restoreDatabaseTables($dbHost, $dbUsername, $dbPassword, $dbName, $filePath){
    // Connect & select the database
    $db = new mysqli($dbHost, $dbUsername, $dbPassword, $dbName);

    // Temporary variable, used to store current query
    $templine = '';

    // Read in entire file
    $lines = file($filePath);

    $error = '';

    // Loop through each line
    foreach ($lines as $line){
        // Skip it if it's a comment
        if(substr($line, 0, 2) == '--' || $line == ''){
            continue;
        }

        // Add this line to the current segment
        $templine .= $line;

        // If it has a semicolon at the end, it's the end of the query
        if (substr(trim($line), -1, 1) == ';'){
            // Perform the query
            if(!$db->query($templine)){
                $error .= 'Error performing query "<b>' . $templine . '</b>": ' . $db->error . '<br /><br />';
            }

            // Reset temp variable to empty
            $templine = '';
        }
    }
    return !empty($error)?$error:true;
}

?>
