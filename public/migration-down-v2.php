<?php

  // Load Craft, taken from index.php
  define('CRAFT_BASE_PATH', dirname(__DIR__));
  define('CRAFT_VENDOR_PATH', CRAFT_BASE_PATH.'/vendor');
  require_once CRAFT_VENDOR_PATH.'/autoload.php';
  if (class_exists('Dotenv\Dotenv') && file_exists(CRAFT_BASE_PATH.'/.env')) {
      (new Dotenv\Dotenv(CRAFT_BASE_PATH))->load();
  }
  define('CRAFT_ENVIRONMENT', getenv('ENVIRONMENT') ?: 'production');
  $app = require CRAFT_VENDOR_PATH.'/craftcms/cms/bootstrap/web.php';

  use craft\db\Query;

  function errorCheck($errors) {
    foreach ($errors as $error) {
      echo $error[0];
    }
  }

  $userSession = Craft::$app->getUser();
  $isAdmin = $userSession->getIsAdmin();
  if ($isAdmin) {
    deleteNewsCategory();
    deletePages();
    deleteSections();
    deleteFields();
    deleteFieldGroups();
  } else {
    echo 'Unauthorized. Make sure you\'re logged in.';
  }

  function deleteNewsCategory() {
    $categoryGroupHandle = 'news';
    $group = Craft::$app->categories->getGroupByHandle($categoryGroupHandle);
    $groupId = $group->id;
    echo $groupId;
    $isDeleted = Craft::$app->categories->deleteGroup($group);
    if ($isDeleted) {
      echo 'News category group deleted';
    }
  }

  function deletePages() {
    $getGlobals = Craft::$app->getGlobals();
    $globalSets = [];
    $globalSets[] = $getGlobals->getSetByHandle('aboutPage');
    $globalSets[] = $getGlobals->getSetByHandle('eventsPage');
    $globalSets[] = $getGlobals->getSetByHandle('newsPage');
    $globalSets[] = $getGlobals->getSetByHandle('resourcesPage');
    $globalSets[] = $getGlobals->getSetByHandle('navigation');
    $globalSets[] = $getGlobals->getSetByHandle('settings');

    foreach ($globalSets as $globalSet) {
      $name = $globalSet['name'];
      $isDeleted = Craft::$app->globals->deleteGlobalSetById($globalSet['id']);
      if ($isDeleted) {
        echo "<br />> {$name} page deleted";
      } else {
        echo "<br />> {$name} page NOT deleted";
      }
    }
  }

  function deleteSections() {
    $getSections = Craft::$app->getSections();
    $sections = [];
    $sections[] = $getSections->getSectionByHandle('events');
    $sections[] = $getSections->getSectionByHandle('news');
    $sections[] = $getSections->getSectionByHandle('speakers');

    foreach ($sections as $section) {
      $name = $section["name"];
      $isDeleted = Craft::$app->sections->deleteSection($section);
      if ($isDeleted) {
        echo "<br />> {$name} section deleted";
      } else {
        echo "<br />> {$name} section NOT deleted";
      }
    }
  }

  function deleteFields() {
    $getFields = Craft::$app->getFields();
    $fields = [];
    $fields[] = $getFields->getFieldByHandle('pageTitle');
    $fields[] = $getFields->getFieldByHandle('bodyText');
    $fields[] = $getFields->getFieldByHandle('image');
    $fields[] = $getFields->getFieldByHandle('steeringGroup');
    $fields[] = $getFields->getFieldByHandle('whyDeclare');
    $fields[] = $getFields->getFieldByHandle('supportUs');
    $fields[] = $getFields->getFieldByHandle('contact');
    $fields[] = $getFields->getFieldByHandle('resources');
    $fields[] = $getFields->getFieldByHandle('usefulLinks');
    $fields[] = $getFields->getFieldByHandle('description');
    $fields[] = $getFields->getFieldByHandle('location');
    $fields[] = $getFields->getFieldByHandle('events');
    $fields[] = $getFields->getFieldByHandle('speakers');
    $fields[] = $getFields->getFieldByHandle('date');
    $fields[] = $getFields->getFieldByHandle('vimeoUrl');
    $fields[] = $getFields->getFieldByHandle('presentationSlides');
    $fields[] = $getFields->getFieldByHandle('categories');
    $fields[] = $getFields->getFieldByHandle('filterLabel');
    $fields[] = $getFields->getFieldByHandle('visible');
    $fields[] = $getFields->getFieldByHandle('aboutVisible');
    $fields[] = $getFields->getFieldByHandle('newsVisible');
    $fields[] = $getFields->getFieldByHandle('eventsVisible');
    $fields[] = $getFields->getFieldByHandle('resourcesVisible');
    $fields[] = $getFields->getFieldByHandle('parentTitle');
    $fields[] = $getFields->getFieldByHandle('parentUrl');
    $fields[] = $getFields->getFieldByHandle('newsletterSignupText');
    $fields[] = $getFields->getFieldByHandle('newsletterLinkText');
    $fields[] = $getFields->getFieldByHandle('newsletterLinkUrl');
    $fields[] = $getFields->getFieldByHandle('siteStatus');

    foreach ($fields as $field) {
      $name = $field["name"];
      $isDeleted = Craft::$app->getFields()->deleteField($field);
      if ($isDeleted) {
        echo "<br />> {$name} field deleted";
      } else {
        echo "<br />> {$name} field NOT deleted";
      }
    }
  }

  function deleteFieldGroups() {
    $groups = [];
    $groups[] = (new Query())
        ->select('id, name')
        ->from('fieldgroups')
        ->where(['name' => 'About'])
        ->one();
    $groups[] = (new Query())
        ->select('id, name')
        ->from('fieldgroups')
        ->where(['name' => 'Events'])
        ->one();
    $groups[] = (new Query())
        ->select('id, name')
        ->from('fieldgroups')
        ->where(['name' => 'Resources'])
        ->one();
    $groups[] = (new Query())
        ->select('id, name')
        ->from('fieldgroups')
        ->where(['name' => 'News'])
        ->one();
    $groups[] = (new Query())
        ->select('id, name')
        ->from('fieldgroups')
        ->where(['name' => 'Navigation'])
        ->one();

    foreach ($groups as $group) {
      $name = $group['name'];
      $isDeleted = Craft::$app->fields->deleteGroupById($group['id']);
      if ($isDeleted) {
        echo "<br />> {$name} field group deleted";
      } else {
        echo "<br />> {$name} field group NOT deleted";
      }
    }
  }

?>
