<?php

  // Load Craft, taken from index.php
  define('CRAFT_BASE_PATH', dirname(__DIR__));
  define('CRAFT_VENDOR_PATH', CRAFT_BASE_PATH.'/vendor');
  require_once CRAFT_VENDOR_PATH.'/autoload.php';
  if (class_exists('Dotenv\Dotenv') && file_exists(CRAFT_BASE_PATH.'/.env')) {
      (new Dotenv\Dotenv(CRAFT_BASE_PATH))->load();
  }
  define('CRAFT_ENVIRONMENT', getenv('ENVIRONMENT') ?: 'production');
  $app = require CRAFT_VENDOR_PATH.'/craftcms/cms/bootstrap/web.php';
  echo " Using database: {$app->config->getDb()->database}";


  $userSession = $app->getUser();
  $isAdmin = $userSession->getIsAdmin();
  if ($isAdmin) {
    echo "<br /> Updating project config...";
    $app->projectConfig->rebuild();
    echo "<br /><br /><strong>Updated!</strong>";
  } else {
    echo '<br /><br />Unauthorized. Make sure you\'re logged in.';
    return false;
  }
?>
